FROM justintclau/magento-php:7.0-fpm-alpine

ARG MAGENTO_ACCESS_KEY_PUBLIC
ARG MAGENTO_ACCESS_KEY_PRIVATE
ARG MAGENTO_CE_VERSION=

ENV MAGENTO_ACCESS_KEY_PUBLIC ${MAGENTO_ACCESS_KEY_PUBLIC}
ENV MAGENTO_ACCESS_KEY_PRIVATE ${MAGENTO_ACCESS_KEY_PRIVATE}
ENV MAGENTO_CE_VERSION ${MAGENTO_CE_VERSION}

RUN apk add --no-cache nginx tini

# Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --install-dir=/usr/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

# Install Magento
RUN COMPOSER_ALLOW_SUPERUSER=1 \
    COMPOSER_AUTH="{\"http-basic\": {\"repo.magento.com\": {\"username\": \"$MAGENTO_ACCESS_KEY_PUBLIC\", \"password\": \"$MAGENTO_ACCESS_KEY_PRIVATE\"}}}" \
    composer create-project \
                --repository-url=https://repo.magento.com/ \
                --prefer-dist \
                magento/project-community-edition /usr/app/magento $MAGENTO_CE_VERSION

# Work around dependency issue
# https://github.com/magento/magento2/issues/8070
RUN sed -i '/"magento\/product-community-edition"/a\ \ \ \ \ \ \ \ "zendframework/zend-stdlib": "2.7.7. as 2.4.11",' /usr/app/magento/composer.json

RUN COMPOSER_ALLOW_SUPERUSER=1 \
    COMPOSER_AUTH="{\"http-basic\": {\"repo.magento.com\": {\"username\": \"$MAGENTO_ACCESS_KEY_PUBLIC\", \"password\": \"$MAGENTO_ACCESS_KEY_PRIVATE\"}}}" \
    composer --working-dir=/usr/app/magento update

# Create a Symlink for Magento CLI
RUN chmod u+x /usr/app/magento/bin/magento \
    && ln -s /usr/app/magento/bin/magento /usr/local/bin/magento

# Config PHP
COPY php/php.ini /usr/local/etc/php/php.ini
COPY php/www.conf /usr/local/etc/php-fpm.d/www.conf

# Configure Nginx
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/conf.d /etc/nginx/conf.d
RUN mkdir -p /run/nginx

# Working dir & startup script
WORKDIR /usr/app
COPY start.sh start.sh
RUN chmod u+x start.sh

EXPOSE 80

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["./start.sh"]
