docker-magento2
===============

This repo builds a Magento 2 CE base image

Magento is run on PHP-FPM 7.0 behind an nginx reverse proxy, exposing port 80 by default.


## Building an app specific image

Magento 2 requires app key and secret to install. Therefore each Magento app requires
their own image. Three environment variables are defined to ease the process:

* `MAGENTO_ACCESS_KEY_PUBLIC` - Generated from Magento Marketplace
* `MAGENTO_ACCESS_KEY_PRIVATE` - Generated form Magento Marketplace
* `MAGENTO_CE_VERSION` - Omit to use the latest; recommended to pin a version in production

```bash
$ ops/build.sh <image>:<tag> <public_key> <private_key> [<version>]
```


## Using Magento CLI

```bash
$ docker run --rm docker-magento magento     # display help
```


## Container File Structure

```
/usr/
  |
  |--bin/composer          # Composer CLI
  |
  |--local/bin/magento     # Magento CLI
  |
  |--app/magento/          # Magento app directory
```
